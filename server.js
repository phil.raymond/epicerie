// Permet de lancer le serveur

// Importation des modules
const express = require('express');
const cors = require('cors');
const itemRouter = require('./src/routers/item')
const mongoose = require("mongoose");

// Configuration du port d'écoute
const port = 3000;

// Création de l'application express
const app = express();

// Activation de CORS
app.use(cors());

// Activation du parser JSON
app.use(express.json());

// Ajout des routes
app.use(itemRouter);

// Connexion à la base de données MongoDB via Mongoose
mongoose
	.connect("mongodb://127.0.0.1:27017/Inventory")
	.then(() => {
		app.listen(port);
		console.log("Serveur à l'écoute sur : http://localhost:" + port);
	});
