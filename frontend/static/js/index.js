// Permet de gérer l'affichage des items de l'inventaire et l'ajout d'items

// Url de l'API
const endpoint = "http://localhost:3000/items";

// Sélection des éléments du DOM
const form = document.querySelector("#form");
const input = document.querySelector("#input");
const inventoryList = document.querySelector("#inventory-list");

/**
 * Fonction qui permet d'afficher un item dans la liste
 * @param {*} item  Objet représentant un item
 */
function displayItem(item) {
  // Création du li
  const listItem = document.createElement("li");
  listItem.classList.add("list-group-item");
  listItem.style.display = "flex";
  listItem.style.alignItems = "center";

  // Création du span
  const itemName = document.createElement("span");
  itemName.textContent = item.name;
  itemName.style.flex = "1";

  // Création du bouton de suppression
  const deleteButton = document.createElement("button");
  deleteButton.classList.add("btn", "btn-danger");
  deleteButton.textContent = "Delete";

  // Gestion du clic sur le bouton de suppression
  deleteButton.addEventListener("click", () => {
    deleteItem(item._id, listItem);
  });

  // Ajout des éléments dans le DOM
  listItem.appendChild(itemName);
  listItem.appendChild(deleteButton);
  inventoryList.appendChild(listItem);
}

/**
 * Fonction qui permet de récupérer tous les items de l'inventaire
 */
fetch(endpoint, { method: "GET" })
  .then((response) => response.json())
  .then((inventory) => {
    // Affiche chaque item de l'inventaire
    inventory.forEach((item) => {
      displayItem(item);
    });
  })
  .catch((error) => {
    console.error(error);
  });

// Gère le formulaire d'ajout d'item
form.addEventListener("submit", (event) => {
  event.preventDefault();
  const itemName = input.value;
  // On valide les données reçues pour créer un nouvel item
  if (!itemName) {
    return alert("Le nom de l'item est obligatoire");
  }

  // Envoie une requête POST à l'API pour ajouter l'item
  fetch(endpoint, {
    method: "POST",
    body: JSON.stringify({ name: itemName }),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((item) => {
      // Affiche l'item dans la liste
      displayItem(item);
      input.value = "";
    })
    .catch((error) => {
      console.error(error);
    });
});

/**
 * Fonction qui permet de supprimer un item de l'inventaire
 * @param {*} itemId ObjectId de l'item
 * @param {*} listItem Objet représentant l'item dans la liste
 */
function deleteItem(itemId, listItem) {
  fetch(`${endpoint}/${itemId}`, {
    method: "DELETE",
  })
    .then((response) => {
      if (response.status === 204) {
        // Supprime l'item de la liste
        listItem.remove();
      }
    })
    .catch((error) => {
      console.error(error);
    });
}
