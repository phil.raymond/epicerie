// Permet de traiter les requêtes items

const Item = require("../models/item");

//Permet de récupérer tous les items de l'inventaire
exports.getItems = (req, res) => {
  Item.find()
    .then((inventory) => {
      // On envoie le résultat de la requête en JSON
      res.status(200).json(inventory);
    })
    .catch((error) => {
      res.status(500).json({ error: error });
    });
};

//Permet d'ajouter un item à l'inventaire
exports.addItem = (req, res) => {
  try {
    try {
      // Validation des données reçues pour créer un nouvel item
      if (!req.body.name) {
        throw new Error("Le nom de l'item est obligatoire");
      }
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
    // Création d'un nouvel item
    const item = new Item(req.body);
    item.save();

    // On envoie le nouvel item en JSON
    res.status(201).json(item);
  } catch (error) {
    res.status(500).json({ error: error });
  }
};

//Permet de retirer un item de l'inventaire
exports.deleteItem = (req, res) => {
  const itemId = req.params.id;
  try {
    // Supprime l'item de la base de données
    Item.findByIdAndRemove(itemId).then((deletedItem) => {
      // Si l'item n'existe pas on renvoie une erreur 404
      if (!deletedItem) {
        return res.status(404).json({
          message: "Item non trouvé",
        });
      }
      res.status(204).end();
    });
  } catch (error) {
    res.status(500).json({ error: error });
  }
};
