// Définition du modèle de données pour les items

const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
    name: String
}, {
    timestamps: true
});

module.exports = mongoose.model('item', itemSchema);