// Permet de gérer les routes pour les items

const express = require('express');

const itemController = require('../controllers/itemController');

const router = express.Router();

// Route pour récupérer tous les items de l'inventaire
router.get('/items', itemController.getItems);

// Route pour ajouter un item à l'inventaire
router.post('/items', itemController.addItem);

// Route pour supprimer un item de l'inventaire
router.delete('/items/:id', itemController.deleteItem);


module.exports = router;
